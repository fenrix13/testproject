﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContosoUniversity.Data;
using ContosoUniversity.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages
{
    public class RegisterModel : PageModel
    {
        private readonly SchoolContext _schoolContext;
        public RegisterModel(SchoolContext schoolContext)
        {
            _schoolContext = schoolContext;
        }
        [BindProperty]
        public Student student { get; set; }
        [BindProperty]
        public Course course { get; set; }
        [BindProperty]
        public Enrollment enrollment { get; set; }
        [BindProperty]
        public INPUT inputtt { get; set; }

        //public enum Grade { A, B, C, D, F }

        public class INPUT 
        {
            public string FName { get; set; }
            public string LName { get; set; }
            public string Course { get; set; }
            public int Credit { get; set; }
            public DateTime EnrollmentDate { get; set; }
            public string Grade { get; set; }
        }

        public async Task<IActionResult> OnPostScheduleAsync()
        {
            if(!ModelState.IsValid)
            {
                return Page();
            }

            Student student = (new Student
            {
                FirstMidName = inputtt.FName,
                LastName = inputtt.LName,
                EnrollmentDate = DateTime.Now,
                
            });

            Course course = (new Course
            {
                CourseID = 8907,
                Title = inputtt.Course,
            });
            Enrollment enrollment = (new Enrollment
            {
                Grade = Grade.A,
                Course = course,
                Student = student,
                StudentID = student.ID,
            });

            //enrollment.Student.FirstMidName = inputtt.FName;
            //enrollment.Student.LastName = inputtt.LName;
            //enrollment.Course.Title = inputtt.Course;
            //enrollment.Course.Credits = inputtt.Credit;
            //enrollment.Student.EnrollmentDate = DateTime.Now;
            //enrollment.Grade = Grade.A;

            //_schoolContext.Students.Add(student);
            //_schoolContext.Courses.Add(course);
            _schoolContext.Add(enrollment);
            await _schoolContext.SaveChangesAsync();
            return RedirectToPage("/Schedule");
        }
    }
}