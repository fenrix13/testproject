﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContosoUniversity.Data;
using ContosoUniversity.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace ContosoUniversity.Pages
{
    public class ScheduleModel : PageModel
    {
        private readonly SchoolContext _schoolContext;
        public ScheduleModel(SchoolContext schoolContext)
        {
            _schoolContext = schoolContext;
        }

        public IList<Enrollment> enrollment { get; set; }

        public async Task OnGetAsync()
        {
            enrollment = await _schoolContext.Enrollments.ToListAsync();
        }
    }
}