﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContosoUniversity.Models
{

    public enum Grade { A, B, C, D, F }

    public class Enrollment
    {
        public int EnrollmentID { get; set; }
        [ForeignKey("Course")]
        public int CourseID { get; set; }
        //[ForeignKey("Student")]
        public int StudentID { get; set; }
        public Grade? Grade { get; set; }
        
        public Course Course { get; set; }
        public Student Student { get; set; }

    }
}
